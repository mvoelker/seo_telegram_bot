from googlesearch import search as g_search
from telegram.ext import Updater, CommandHandler, CallbackContext
import logging
import time
import sys

class Logger:
    """
    Logger class for logging
    """
    LOG_FORMAT = '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s'
    def __init__(self, instance_name):
        self.instance_name = instance_name
        self.logger = logging.getLogger(instance_name)
        self.set_logger_settings()

    def set_logger_settings(self):
        handler = logging.StreamHandler()
        formatter = logging.Formatter(self.LOG_FORMAT)
        handler.setFormatter(formatter)
        logging.basicConfig(filename=f"debug_{self.instance_name}.log",
                            filemode='a',
                            format=self.LOG_FORMAT,
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)


class SearchInGoogle:
    """
    Class for search in the google ranking for the specific website
    """
    def __init__(self, logger, tld="de", lang="de"):
        """
        The language german and location germany is default, this could be configurable from outside
        :param logger: Logger,  class for logging
        :param tld: string, location for google ranking country
        :param lang: string, specific language
        """
        self.tld = tld
        self.lang = lang
        self.logger = logger

    def search(self, url_to_search, keys):
        """
        This is the search method to get the result from google
        :param url: string, which describe the url to find
        :param keys: string, which are the keywords for the google search
        :return: tuple of string, if they empty nothing was found
        """
        self.logger.info(f"Search for {url_to_search} with {keys}")
        for idx, url in enumerate(g_search(keys, tld=self.tld, lang=self.lang, start=0, stop=250), start=1):
            self.logger.info(f"{idx} - {url}")
            if url_to_search.lower() in url.lower():
                found_no = idx
                self.logger.info(f"Found {url} on {found_no}")
                return found_no, url
            time.sleep(1)
        return "", ""

class SeoBot:
    """
    Class of the telegram seo bot. This class should only has one instance, otherwise update conflict on the telegram
    server will follow
    """
    def __init__(self, logger, search_in_google, bot_token):
        self.logger = logger
        self.search_in_google = search_in_google
        # create the update to communicate with the user
        self.updater = Updater(bot_token, use_context=True)
        # interval of getting updates from the bot of the given conten from list_of_interest
        self.interval = 3600 * 3
        # list of website with keywords which will be send every self.interval an update
        self.list_of_interest = []
        # the chat will be set if the cmd /start is calling in the chat
        self.chat_id = None
        # read files from the list_of_interest file
        self._read_file()
        # add the commands for the communication
        self._init_bot()
        # set the bot into idle
        self.run()

    def _init_bot(self):
        """
        Method is used to add the command to the chat bot
        """
        self.logger.info("Init bot")
        self.updater.dispatcher.add_handler(CommandHandler('seo', self.seo))
        self.updater.dispatcher.add_handler(CommandHandler('start', self.start))


    def _read_file(self):
        """
        Method to the read the content from the list_of_interest file.
        The notation of the file should:
        "url1","keyword1 keyword2"
        "url2","keyword1 keyword2"
        """
        with open("list_of_interest") as f:
            lines = f.readlines()
        _ = [self.list_of_interest.append(eval(line)) for line in lines]
        self.logger.info(f"Read file list_of_interest: {self.list_of_interest}")

    def run(self):
        """
        Method to set the chat bot into idle an let him communicate with the user
        """
        self.logger.info("Set bot to idle")
        self.updater.start_polling()
        self.updater.idle()

    def callback(self, context: CallbackContext):
        """
        This callback will be called every self.interval seconds
        :param context: CallbackContext, to use the methods of the bot
        """
        for item in self.list_of_interest:
            self.logger.info(f"Get info about {item}")
            msg_to_send = self.send_result(*item)
            self.logger.info(f"Send {msg_to_send} with {item}")
            context.bot.send_message(chat_id=self.chat_id, text=msg_to_send, parse_mode='Markdown')

    def start(self, bot, _):
        """
        This method is needed to get the chat_id from the user to interact with him
        :param bot: Bot, which is the telegram bot itself
        """
        self.chat_id = bot["message"].chat_id
        self.logger.info(f"Get chat id: {self.chat_id}")
        bot.message.reply_text(f"Thank you for the id. The callback job will be run every {self.interval} and will search "
                               f"for these content {self.list_of_interest}")
        self.updater.job_queue.run_repeating(self.callback, interval=self.interval, first=0)

    def seo(self, bot, _):
        """
        Method to get a info about a website and the ranking with the command /seo in the chat.
        We need this notation:
        /seo your_url.com|keyword1 keyword2
        :param bot: bot: Bot, which is the telegram bot itself
        """
        msg = bot.message.text.replace("/seo", "")
        url, keys = msg.split("|")
        url, keys = url.lstrip().rstrip(), keys.lstrip().rstrip()
        bot.message.reply_text(f"Start searching for {url} with {keys}")
        msg_to_send = self.send_result(keys, url)
        bot.message.reply_text(msg_to_send, parse_mode='Markdown')

    def send_result(self, keys, url):
        """
        Method to generate the message string
        :param keys: String, with keywords
        :param url: String, with the url
        :return: string, message string
        """
        result, found_url = self.search_in_google.search(url, keys)
        msg_to_send = f"*{found_url}* on position *{result}* with *{keys}*" if result else f"*{url}* not found with *{keys}*"
        return msg_to_send

if __name__ == "__main__":

    main_logger = Logger("Main").logger
    # we except an argument which is the bot token
    if len(sys.argv) != 2:
        main_logger.error("A telegram bot token is needed")
        sys.exit(1)
    telegram_bot_token = sys.argv[1]
    main_logger.info(f"Telegram bot token is {telegram_bot_token}")
    # instance of the search class
    search_in_google = SearchInGoogle(Logger("searchGoogle").logger)
    # instance of the bot class
    SeoBot(Logger("telegram").logger, search_in_google, telegram_bot_token)
