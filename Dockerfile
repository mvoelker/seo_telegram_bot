FROM alpine:3.10.1
RUN apk update 
RUN apk add python3 py3-pip python3-dev libffi-dev build-base openssl-dev bash
RUN pip3 install python-telegram-bot google
WORKDIR /root
ARG TOKEN
ADD list_of_interest list_of_interest
ADD bot.py bot.py
RUN echo "python3 bot.py ${TOKEN}" > entry.sh
RUN chmod +x ./entry.sh
ENTRYPOINT bash ./entry.sh