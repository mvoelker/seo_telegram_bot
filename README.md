## How to use the program
### Telegram
#### Installation
You have to install the telegram app.  

Android: [Playstore](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=de&referrer=utm_source%3Dgoogle%26utm_medium%3Dorganic%26utm_term%3Dplay+store+telegram&pcampaignid=APPU_1_c4DRXeKADMvPwQKnz6Zg) 
or better [Fdroid](https://f-droid.org/app/org.telegram.messenger
)  
iPhone:
[AppStore](https://apps.apple.com/de/app/telegram-messenger/id686449807)

#### Create a bot
It is necessary to create a bot for you. You could use this tutorial [Create bot](https://core.telegram.org/bots#creating-a-new-bot). You will get a token and the token is needed to start the bot program. The token has got this notation:  
  
 **110201543:AAHdqTcvCH1vGWJxfSeofSAs0K5PALDsaw**

#### Set token
Now you have to set your token into this file *buildDockerImages.sh*. The token has to be replaced in this line:  
      
`docker build --build-arg TOKEN=your_bot_token -t seo_telegram_bot .`  
    
The string **your_bot_token** must be replaced. 

### Docker
#### Why docker
If you would like to use docker, you will have to install it. My bot is running on a Synology NAS and it can be easily started without a chroot on the NAS. A further advantage is the **restart=always** function of the container call. It means the container will restart after an error or a reboot on its own. The other way is to use a python3 environment. The required packages are:   
- python-telegram-bot 
- google

#### Start container
You could easily use the *restart.sh*. 

### Telegram app
#### Command
The application has only one command:   
  
**/seo**
  
This command can check an URL with specific keywords for you. The notation is important!.  
  
**/seo myExampleSite|keyword1 keyword2**
  

![image](static/example_gitlab.jpg)
 
#### Feed
There is another useful automation: The file 
 *list_of_interest* includes URLs and keywords which will be crawled continuously, every three hours you will get a message - feel free to change the time in the *bot.py* file. The notation of the *list_of_interest* is:  
  
```
"url1","keyword1 keyword2"
"url2","keyword1 keyword2"
```

**This function has to be enabled!!**  
  
It is needed to set your chat id with this command:  
  
**/start**  
 
#### Website
Further information you could find here: 
[software-and-testing.de](https://software-and-testing.de/seo)
